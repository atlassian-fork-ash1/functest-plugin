package com.atlassian.functest.test3;

import org.junit.Test;

public abstract class AbstractTestClass {
    private AbstractTestClass() {
    }

    @Test
    public void testInAbstractClass() {
    }
}
