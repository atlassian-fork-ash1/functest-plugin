package com.atlassian.functest.junit;

import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;

/**
 * This class should be extended by any tests requiring Spring components be injected into them at test time.
 * <p>
 * Currently, setter injection is supported.
 */
@RunWith(SpringAwareJUnit4ClassRunner.class)
public abstract class SpringAwareTestCase {

    @Before
    public void setUp() throws Exception {

    }

    @After
    public void tearDown() throws Exception {

    }
}
