package com.atlassian.functest.client;

import java.util.List;

interface AnnotationValidator {

    List<Throwable> getErrors();

}
