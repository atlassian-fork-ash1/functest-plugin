package com.atlassian.functest.client;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

class MethodValidator {

    static Collection<? extends Throwable> validate(Class<?> clazz, Method fMethod) {

        List<Throwable> errors = new ArrayList<>();

        if (!Modifier.isStatic(fMethod.getModifiers())) {
            errors.add(new Exception("Method " + fMethod.getName() + "() should be static"));
        }

        if (!Modifier.isPublic(fMethod.getDeclaringClass().getModifiers())) {
            errors.add(new Exception("Class " + fMethod.getDeclaringClass().getName() + " should be public"));
        }

        if (!Modifier.isPublic(fMethod.getModifiers())) {
            errors.add(new Exception("Method " + fMethod.getName() + "() should be public"));
        }

        if (fMethod.getReturnType() != clazz) {
            errors.add(new Exception("Method " + fMethod.getName() + "() should be " + clazz.getName()));
        }

        return errors;
    }
}
