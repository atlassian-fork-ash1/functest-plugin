package com.atlassian.functest.api;

/**
 *
 */
public enum ProductType {
    CONFLUENCE,
    JIRA,
    REFAPP,
    BAMBOO,
    FECRU
}
