package com.atlassian.functest.rest;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * Describes a single test on the remote system
 * <p> That is corresponds to a single Junit tests class. Description contains record for each method annotated as a test
 *
 * @author pandronov
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class TestDescription implements Iterable<String> {
    /**
     * Name of the class contains test methods
     */
    @XmlAttribute(name = "class")
    private
    @NotNull
    String clazz;

    /**
     * Name of methods annotated as tests
     */
    @XmlElement
    private
    @NotNull
    List<String> tests = new ArrayList<String>();

    /**
     * Create empty TestDescription
     */
    // special for JAXB
    public TestDescription() {

    }

    /**
     * Creates new test description
     *
     * @param clazz test name
     * @param tests list of tests in the class
     */
    public TestDescription(String clazz, List<String> tests) {
        this.clazz = clazz;
        this.tests = new ArrayList<String>(tests);
    }

    /**
     * Return name of the class contains tests
     *
     * @return class name
     */
    public String getTestClass() {
        return clazz;
    }

    /**
     * Return iterator over names of tests in the curret test class. Note that order in which method apears in the
     * iterator is not defined
     *
     * @return tests iterator
     */
    @Override
    public Iterator<String> iterator() {
        return Collections.unmodifiableList(tests).iterator();
    }
}
