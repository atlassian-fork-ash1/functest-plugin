package com.atlassian.functest.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

import javax.annotation.Nonnull;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.io.InputStream;

/**
 */
public class PluginMetaData extends DefaultHandler {

    private final String pluginKey;
    private Logger LOG = LoggerFactory.getLogger("atlassian.plugin");
    private static final PluginMetaData INSTANCE = new PluginMetaData();

    public static PluginMetaData instance() {
        return INSTANCE;
    }

    private PluginMetaData() {

        pluginKey = getPluginKeyForClass(getClass());
    }

    public
    @Nonnull
    String getPluginKeyForClass(Class clazz) {
        try {
            InputStream in = clazz.getResourceAsStream("/atlassian-plugin.xml");
            if (in == null) {
                LOG.warn("Resource /atlassian-plugin.xml not found. Meta-data will be empty.");
                throw new IllegalArgumentException("atlassian-plugin.xml not found");
            }
            SAXParserFactory spf = SAXParserFactory.newInstance();
            spf.setValidating(false);
            SAXParser saxParser = spf.newSAXParser();
            XMLReader reader = saxParser.getXMLReader();
            DescriptorHandler handler = new DescriptorHandler();
            reader.setContentHandler(handler);
            reader.parse(new InputSource(in));
            return handler.getPluginKey();
        } catch (ParserConfigurationException e) {
            LOG.warn("Error trying to parse plugin meta-data: " + e.getMessage(), e);
            throw new IllegalArgumentException(e);
        } catch (SAXException e) {
            LOG.warn("Error trying to parse plugin meta-data: " + e.getMessage(), e);
            throw new IllegalArgumentException(e);
        } catch (IOException e) {
            LOG.warn("Error trying to parse plugin meta-data: " + e.getMessage(), e);
            throw new IllegalArgumentException(e);
        }
    }

    public String getPluginKey() {
        return pluginKey;
    }

    public Logger getLogger() {
        return LoggerFactory.getLogger("atlassian.plugin." + getPluginKey());
    }

    private static final class DescriptorHandler extends DefaultHandler {
        private String pluginKey;

        @Override
        public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
            if (qName.equalsIgnoreCase("atlassian-plugin")) {
                pluginKey = attributes.getValue("key");
            }
        }

        public String getPluginKey() {
            return pluginKey;
        }
    }


}
