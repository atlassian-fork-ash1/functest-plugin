package com.atlassian.functest.util;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class PluginMetaDataTest {

    @Test
    public void testGetPluginKey() {
        PluginMetaData data = PluginMetaData.instance();
        assertEquals(translate("${project.groupId}.${project.artifactId}"), translate(data.getPluginKey()));
    }

    @Test
    public void testGetLogger() {
        PluginMetaData data = PluginMetaData.instance();
        assertEquals(translate("atlassian.plugin.${project.groupId}.${project.artifactId}"), translate(data.getLogger().getName()));
    }

    private String translate(String orig) {
        return orig.replace("${project.groupId}", "com.atlassian.functest").replace("${project.artifactId}", "functest-plugin");
    }
}
